/*
  ==============================================================================

    SynthVoice.h
    Created: 10 May 2020 7:30:39pm
    Author:  tcari

  ==============================================================================
*/

#pragma once
#include <JuceHeader.h>
#include "SynthSound.h"

class SynthVoice : public SynthesiserVoice 
{
public:
    bool canPlaySound(SynthesiserSound* sound)
    {
        return true;
    }

    //==========================================

    void startNote(int midiNodeNumber, float velocity, SynthesiserSound* sound, int currentPitchWheelPosition)
    {

    }

    //==========================================

    void stopNote(float velocity, bool allowTailOff)
    {

    }

    //==========================================

    void pitchWheelMoved(int newPitchWheelValue)
    {

    }

    //==========================================

    void controllerMoved(int controllerNumber, int newControllerValue)
    {

    }

    //==========================================

    void renderNextBlock(AudioBuffer<float>& outputBuffer, int startSample, int numSamples)
    {

    }

    //==========================================

private:

};